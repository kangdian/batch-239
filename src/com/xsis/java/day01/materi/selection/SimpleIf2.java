package com.xsis.java.day01.materi.selection;

import java.util.Scanner;

public class SimpleIf2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter an integer : ");
        int number = scan.nextInt();

        boolean result;
        if (number % 2 == 0){
            result = true;
        }else{
            result = false;
        }

        System.out.println("Result  : "+result);
    }
}
