package com.xsis.java.day01.materi.selection;

import java.util.Scanner;

public class ComputeKwh {
    public static void main(String[] args) {
        // compute kwh with progressive rate
        /*
            kWh <= 500 -> $0.10
            kWh 501 <= kWh < 2000 -> $0.25
            kwh 2001 <= kWh < 4000 -> $0.4
            4001 < kWh -> $0.6
         */
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number kWh : ");
        int kwh = scan.nextInt();
        double pay;

        if (kwh <= 500){
            pay = kwh * 0.10;
        }else if (kwh <= 2000){
            pay = 500 * 0.10 + (kwh-500) * 0.25;
        }else if (kwh <= 4000){
            pay = 500 * 0.10 + 1500 * 0.25 + (kwh - 2000) * 0.40;
        }else {
            pay = 500 * 0.10 + 1500 * 0.25 +   2000 * 0.40 + (kwh - 4000)* 0.60;
        }
        System.out.println("Total paymen yang harus dibayar : "+pay);

    }
}
