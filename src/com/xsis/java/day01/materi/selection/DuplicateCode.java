package com.xsis.java.day01.materi.selection;

public class DuplicateCode {
    public static void main(String[] args) {
        double harga = 50,total=0, tax = 0.5,tips=10;
        if ( harga >= 100){
            total = harga+ (harga * tax)+tips;
            System.out.println("Total : "+total);
        }else{
            total = harga+ (harga * tax);
            System.out.println("Total : "+total);
        }

        // right code
        System.out.println("Total : "+total);
    }
}
