package com.xsis.java.day01.materi.selection;

public class MultipleIf {
    public static void main(String[] args) {
        int i = 1, j=2, k=3;
        if ( i > j){
            if (i > k)
                System.out.println("A");
        }else{
            System.out.println("B");
        }
    }
}
