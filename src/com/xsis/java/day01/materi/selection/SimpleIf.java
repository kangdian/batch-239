package com.xsis.java.day01.materi.selection;

import java.util.Scanner;

public class SimpleIf {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter an integer : ");
        int number = scan.nextInt();

        if (number % 5 == 0){
            System.out.println("Kelipatan 5");
        }

        if (number % 2 == 0){
            System.out.println("Kelipatan 2");
        }
    }
}
