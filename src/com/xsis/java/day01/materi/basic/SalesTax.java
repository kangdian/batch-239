package com.xsis.java.day01.materi.basic;

import java.util.Scanner;

public class SalesTax {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter purchase amount : ");
        double amount = scan.nextDouble();

        double tax = amount * 0.06;
        System.out.println("Result after pajak : "+ (tax * 100)/100.0);
    }
}
