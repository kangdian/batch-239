package com.xsis.java.day01.materi.basic;

import java.util.Scanner;

public class ComputeAreaWithConstant {
    public static void main(String[] args) {
        final double PI = 3.14159;

        // create scanner object
        Scanner input = new Scanner(System.in);


        System.out.println("Enter radius : ");
        double radius = input.nextDouble();
        // step 2 : hitungluas lingkaran
        double area = radius * radius * PI;
        // step 3 : luas lingkaran
        System.out.println("Luas linkarang dengan radius : "+radius+" adalah : "+area);
        System.out.printf("Luas lingkaran ber radius %s adalah : %.3f",radius,area);
    }
}
