package com.xsis.java.day01.materi.basic;

import java.util.Scanner;

public class ComputeAverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input 3 bilangan : ");
        double num1 = scanner.nextDouble();
        double num2 = scanner.nextDouble();
        double num3 = scanner.nextDouble();

        // compute average
        double avarage = (num1 + num2+ num3)/3;

        // display result
        System.out.printf("Result : %.2f",avarage);
    }
}
