package com.xsis.java.day01.materi.basic;

import java.util.Scanner;

public class DisplayTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // prompt user to input
        System.out.println("Enter integer for seconds : ");
        int seconds = scanner.nextInt();

        int minutes = seconds / 60;
        int sisa = seconds % 60;
        System.out.printf("Result : %s minutes %s detik",minutes,sisa);

    }
}
