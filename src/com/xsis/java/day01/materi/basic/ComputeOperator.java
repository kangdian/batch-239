package com.xsis.java.day01.materi.basic;

public class ComputeOperator {
    public static void main(String[] args) {
        double x = 5,y=3,z=2;
        double a = (x+y)/z;
        double b = x + y /z;
        double c = x +(y * 2)/z;
        System.out.println("Result a: "+a);
        System.out.println("Result b: "+b);
        System.out.println("Result c: "+c);

        // assignment operator
        x += 5;
        System.out.println("x+= "+x);

        y -= 1;
        System.out.println("y-= : "+y);
        z *= 2;
        System.out.println("z *=: "+z);

        z /= 2;
        System.out.println("z /=: "+z);

        z %= 2;
        System.out.println("z %=: "+z);

        // increment
        double d = 1;
        d++;
        System.out.println("d++ = "+d);
        d--;
        System.out.println("d-- = "+d);

        // pangkat
        int h=3;
        h=2^(3);
        System.out.println("h : "+h);
    }
}
