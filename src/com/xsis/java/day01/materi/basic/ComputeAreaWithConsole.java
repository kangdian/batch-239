package com.xsis.java.day01.materi.basic;

import java.util.Scanner;

public class ComputeAreaWithConsole {
    public static void main(String[] args) {
        // create scanner object
        Scanner input = new Scanner(System.in);

        System.out.println("Enter radius : ");
        double radius = input.nextDouble();
        // step 2 : hitungluas lingkaran
        double area = radius * radius * 3.14159;
        // step 3 : luas lingkaran
        System.out.println("Luas linkarang dengan radius : "+radius+" adalah : "+area);
        System.out.printf("Luas lingkaran ber radius %s adalah : %.3f",radius,area);
    }
}
