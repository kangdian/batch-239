package com.xsis.java.day01.materi.basic;

public class ComputeArea {
    public static void main(String[] args) {
        // hitung luas area lingkaran
        double radius; // declare radius
        double area;
        // step 1 : read in radius
        radius = 20;

        // step 2 : hitung luas nya
        area = radius * radius * 3.14159;

        // step 3 : display area
        System.out.println("Luas lingkaran ber radius :  "+radius+", adalah "+area);
    }
}
