package com.xsis.java.day01.quiz.selection;

import java.util.Scanner;

public class TopUpPulsa {
    public static void main(String[] args) {

            Scanner scan = new Scanner(System.in);
            System.out.print("Input isi pulsa : ");
            double money = scan.nextDouble();
            int point=0;
            if (money <= 10000){
                point = 0;
            }else if ( money <= 30000){
                point = (int)((money - 10000)/1000);
            }else {
                point = (int)((10000/1000) +  ((money- 20000)/1000) + (money - 30000)/1000);
            }
            System.out.println("Total point : "+point);

    }
}
