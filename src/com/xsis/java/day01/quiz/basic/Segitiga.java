package com.xsis.java.day01.quiz.basic;

import java.util.Scanner;

public class Segitiga {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Entry Sudut ke-1 : ");
        int s1 = scan.nextInt();
        System.out.print("Entry Sudut ke-2 : ");
        int s2 = scan.nextInt();

        int s3 = 180 - (s1+s2);
        System.out.println("Sudut ke-3 : "+s3);
    }
}
