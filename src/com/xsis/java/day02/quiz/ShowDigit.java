package com.xsis.java.day02.quiz;

import java.util.Scanner;

public class ShowDigit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x, i, digit3, r, digit2, digit1;
        System.out.print("Enter a digit 0 - 9: ");
        x = scan.nextInt();
        for (i = 100; i <= 999; i++) {
            digit3 = (int) (i / 100);
            r = i % 100;
            digit2 = (int) (r / 10);
            digit1 = r % 10;
            if (digit3 == x || digit2 == x || digit1 == x) {
                System.out.println(i);
            }
        }
    }
}
