package com.xsis.java.day02.quiz;

import java.util.Scanner;

public class CariKata {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s, temp_s, letter;
        boolean found;
        System.out.println("Input Kata : ");
        s = scan.nextLine();
        System.out.print("Cari Karakter: ");
        letter = scan.next();
        found = false;
        for (int i = 0; i <= s.length() - 1; i++) {
            temp_s = "" + s.charAt(i);
            if (temp_s.equals(letter) == true) {
                found = true;
            }
        }
        if (found == true) {
            System.out.println("Character " + letter + " found!");
        }
    }
}
