package com.xsis.java.day02.quiz;

import java.util.Scanner;

public class CountWord {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int characters, count, i, words;
        String msg, character;
        System.out.print("Enter a message: ");
        msg = input.nextLine();
        characters = msg.length();
        count = 0;
        for (i = 0; i <= characters - 1; i++) {
            character = "" + msg.charAt(i);
            if (character.equals(" ") == true) {
                count++;
            }
        }
        words = count + 1;
        System.out.println("The message entered contains " + words + " words");
    }
}
