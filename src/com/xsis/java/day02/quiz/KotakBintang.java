package com.xsis.java.day02.quiz;

import java.util.Scanner;

public class KotakBintang {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter an integer between 3 and 20: ");
        int n = scan.nextInt();
        for (int j = 1; j <= n; j++) {
            System.out.print("* ");
        }
        System.out.println();
        for (int i = 1; i <= n - 2; i++) {
            System.out.print("* ");
            for (int j = 1; j <= n - 2; j++) {
                System.out.print("  ");
            }
            System.out.println("* ");
        }
        for (int j = 1; j <= n; j++) {
            System.out.print("* ");
        }
    }
}
