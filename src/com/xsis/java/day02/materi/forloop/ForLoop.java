package com.xsis.java.day02.materi.forloop;

import java.util.Scanner;

public class ForLoop {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number=0, max=0;

        for (int i = 0; i < 4 ; i++) {
            System.out.print("Enter angka ke "+i+" : ");
            number = input.nextInt();
            if (number > max)
                max = number;
        }
        System.out.println("max is " + max);
        System.out.println("number " + number);
    }
}
