package com.xsis.java.day02.materi.whiledo;

public class WhileDo {
    public static void main(String[] args) {
        int count = 0;
        while (count < 100) {
            System.out.println("Welcome to Java!");
            count++;
        }

    }
}
