package com.xsis.java.day02.materi.whiledo;

import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
         // Generate a random number to be guessed
         int number = (int)(Math.random() * 21);

         Scanner scan = new Scanner(System.in);
         System.out.println("Guess a magic number between 0 and 20");

         int guess = -1;
         while (guess != number) {
             // Prompt the user to guess the number
             System.out.print("\nEnter your guess: ");
             guess = scan.nextInt();

             if (guess == number)
                 System.out.println("Yes, the number is " + number);
             else if (guess > number)
                 System.out.println("Your guess is too high");
             else
             System.out.println("Your guess is too low");
             } // End of loop
    }
}
