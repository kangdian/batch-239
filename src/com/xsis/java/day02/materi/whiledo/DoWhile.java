package com.xsis.java.day02.materi.whiledo;

import java.util.Scanner;

public class DoWhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number, max;
        System.out.print("Enter integer (0 - exit) :");
        number = input.nextInt();
        max = number;
        do  {
            System.out.print("Enter integer (0 - exit) :");
            number = input.nextInt();
            if (number > max)
                max = number;
        }while(number != 0);
        System.out.println("max is " + max);
        System.out.println("number " + number);
    }
}
