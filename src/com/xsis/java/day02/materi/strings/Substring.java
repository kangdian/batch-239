package com.xsis.java.day02.materi.strings;

public class Substring {
    public static void main(String[] args) {
        String s = "Welcome to Java";
        String m = s.substring(0,11) + "Python";
        System.out.println(m);

        System.out.println(s.indexOf('W'));
        System.out.println(s.indexOf('t'));
        System.out.println(s.indexOf("C",1));
    }
}
