package com.xsis.java.day02.materi.strings;

public class String01 {
    public static void main(String[] args) {
        String s = "Java";
        // length string
        System.out.println("Panjang string : "+s.length());


        // chartAt -> konversi ke char
        char c = s.charAt(1);
        System.out.println("c : "+c);
        String reverse = s.charAt(3)+""+s.charAt(2)+""+s.charAt(1)+""+s.charAt(0);
        System.out.println("Reverse : "+reverse);

        // concat
        String concat = s + " Bootcamp";
        System.out.println("concat : "+concat);

        // toUppercase
        String upperCase = s.toUpperCase();
        System.out.println("uppercase : "+upperCase);

        // tolowercase
        String lowerCase = s.toLowerCase();
        System.out.println("lowercase : "+lowerCase);

        // trim
        String a,b;
        a = " Hello ";
        b = a.trim();
        System.out.println(b + " Java!");
        // replace
        String d, e;
        d = "I am newbie in C++. C++ rocks!";
        e = d.replace("C++", "Java");
        System.out.println(e);



    }
}
