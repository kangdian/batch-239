package com.xsis.java.day02.materi.strings;

import java.util.Scanner;

public class NextLineString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter karakter : ");
        String s = scan.nextLine();
        System.out.println("s : "+s);
        char c = s.charAt(2);
        System.out.println("c : "+c);
    }
}
