package com.xsis.java.day02.materi.strings;

import java.util.Scanner;

public class NextString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter separated word by spaces : ");
        String s1 = scan.next();
        String s2 = scan.next();
        String s3 = scan.next();
        System.out.println("s1 : "+s1);
        System.out.println("s2 : "+s2);
        System.out.println("s3 : "+s3);
    }
}
