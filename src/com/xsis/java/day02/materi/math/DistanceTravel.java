package com.xsis.java.day02.materi.math;

import java.util.Scanner;

public class DistanceTravel {
    /*
        kasus jika rumus menghitung jarak adala 𝑆= 𝑢𝑜+ 12𝑎𝑡2 (at kuadrat)
        maka hitung waktu (t) yang ditempuh dengan inputan jarak, dan kecepatan
        rumus t = akar 2s/a
    */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double s, a, t;
        System.out.println("Enter accelaration : ");
        a = scan.nextDouble();
        System.out.println("Enter distance : ");
        s = scan.nextDouble();
        t = Math.sqrt(2 * s /a);
        System.out.println("Waktu yang ditempuh : "+t);
    }
}
