package com.xsis.java.day02.materi.math;

public class MathOperation {
    public static void main(String[] args) {
        double a = Math.pow(2,3);
        System.out.println("pangkat dari = "+a);

        double x = Math.sqrt(4);
        System.out.println(" akar dari = "+x);

        double f = Math.floor(34.1567);
        System.out.println("floor = "+f);

        double c = Math.ceil(2.5);
        System.out.println("ceil = "+c);

        double r = Math.round(-2.5);
        System.out.println("round = "+r);
    }
}
